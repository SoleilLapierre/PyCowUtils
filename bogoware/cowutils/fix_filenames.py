#!/usr/bin/python3
"""Canonicalize file and directory names using a sequence of regexes."""

# This script canonicalizes file and directory names by applying a
# sequence of regular expression replacements.

# This is useful for fixing the horribly wrong naming conventions used
# by many people who share files via various file sharing networks.

# While this cannot completely replace manual bulk rename tools, it
# does take care of most of the work, most of the time.

import os
import sys
import glob
import re
from typing import Sequence, Any

g_preview_mode: bool = False
g_recursive_mode: bool = False
g_fix_dirs: bool = False
g_fix_collisions: bool = False

g_rules = \
    [
        [r"[\[\(\{\<][0-9A-Fa-f]{16}[\]\}\)\>]", ""],
        [r"[\[\(\{\<][0-9A-Fa-f]{8}[\]\}\)\>]", ""],
        [r"\. ", "_"],
        [r"\.(?=.*\.)", "_"],
        ["([0-9]{4})-([0-9]{2})-([0-9]{2})", r"\\1\\2\\3"],
        ["([0-9]{2})-([0-9]{2})-([0-9]{4})", r"\\3\\2\\1"],
        ["___", "__"],
        [" $", ""],
        [r"\&", "_and_"],
        [r"\+", "_plus_"],
        [r"\@", "_AT_"],
        [r"\?", ""],
        [r"\!", ""],
        [r"\,", ""],
        [r"\'", ""],
        [r"\~", "_"],
        [r"\:", " "],
        [r"\`", ""],
        [r"\"", ""],
        [r"\#", ""],
        [r"\$", ""],
        [r"\%", ""],
        [r"\^", ""],
        [r"\(", "_"],
        [r"\)", ""],
        [r"\[", "_"],
        [r"\]", ""],
        [r"\{", "_"],
        [r"\}", ""],
        [r"\<", "_"],
        [r"\>", ""],
        [r"\;", ""],
        [r"\=", "_"],
        [r"\|", ""],
        [" - ", "_"],
        ["-", "_"],
        [" ", "_"],
        ["([a-z])_Of_", r"\\1_of_"],
        ["([a-z])_And_", r"\\1_and_"],
        ["([a-z])_The_", r"\\1_the_"],
        ["([a-z])_To_", r"\\1_to_"],
        ["([a-z])_Or_", r"\\1_or_"],
        ["([a-z])_In_", r"\\1_in_"],
        ["([a-z])_For_", r"\\1_for_"],
        ["([a-z])_A_", r"\\1_a_"],
        ["([a-z])_On_", r"\\1_on_"],
        ["([a-z])_At_", r"\\1_at_"],
        ["([a-z])_That_", r"\\1_that_"],
    ]


def _print_usage() -> None:
    print("Usage: fix_filenames [-d] [-h] [-p] [-r] filespec [filespec] ...")
    print(" -d: Fix directory names also.")
    print(" -h: Prints this message")
    print(" -p: Preview mode; prints what would be done otherwise.")
    print(" -r: Recursive operation")
    print(" -#: Try to fix name collisions by appending a number.")


def _parse_command_line(args: Sequence[str]) -> Any:
    global g_preview_mode, g_recursive_mode, g_fix_dirs, g_fix_collisions
    files = []
    count = len(args)
    i = 1

    while i < count:
        switch = args[i]
        i += 1
        if (switch == "-h") or (switch == "-help"):
            _print_usage()
            sys.exit(0)
        elif switch == "-r":
            g_recursive_mode = True
        elif switch == "-p":
            g_preview_mode = True
        elif switch == "-#":
            g_fix_collisions = True
        elif switch == "-d":
            g_fix_dirs = True
        else:
            files.append(switch)

    return files


def _fix_name(old_name: str) -> str:
    global g_rules
    name = old_name
    for pair in g_rules:
        pattern = pair[0]
        replacement = pair[1]
        prev = None
        while name != prev:
            prev = name
            name = re.sub(pattern, replacement, prev)

    return name


def _insert_number(file_path: str, num: int) -> str:
    dir_name, file = os.path.split(file_path)
    if "." in file:
        fname, ext = file.rsplit(".", 1)
        fname += "_" + str(num)
        file = fname + "." + ext
    else:
        file += "_" + str(num)

    return os.path.join(dir_name, file)


def _do_rename(old_path: str, new_path: str) -> None:
    global g_preview_mode, g_fix_collisions
    print(old_path + " -> " + new_path)
    if os.path.exists(new_path):
        success = False
        print("ENCOUNTERED A NAME COLLISION!")
        if g_fix_collisions:
            for i in range(2, 1000):
                temp_path = _insert_number(new_path, i)
                if not os.path.exists(temp_path):
                    print("RESOLVING USING " + temp_path + " INSTEAD OF " + new_path)
                    new_path = temp_path
                    success = True
                    break

        if not success:
            sys.exit(1)

    if not g_preview_mode:
        os.rename(old_path, new_path)


def _enumerate(dir_path: str) -> Sequence[str]:
    result = []
    for name in os.listdir(dir_path):
        child_path = os.path.join(dir_path, name)
        result.append(child_path)

    return result


def _fix(path_spec: str):
    global g_recursive_mode, g_fix_dirs
    matches = [path_spec]
    if os.path.isdir(path_spec) or ("?" in path_spec) or ("*" in path_spec):
        try:
            matches = glob.glob(path_spec)
        except BaseException:  # pylint: disable=broad-except
            print("Error globbing: " + path_spec)
            sys.exit(1)

    for candidate in matches:
        if os.path.isfile(candidate) and not os.path.isdir(candidate):
            (dir_name, filename) = os.path.split(candidate)
            new_file_name = _fix_name(filename)
            if new_file_name != filename:
                new_path = os.path.join(dir_name, new_file_name)
                _do_rename(candidate, new_path)

        elif os.path.isdir(candidate):
            if g_recursive_mode:
                contents = _enumerate(candidate)
                for new_path in contents:
                    _fix(new_path)

            if g_fix_dirs:
                (dir_name, filename) = os.path.split(candidate)
                new_file_name = _fix_name(filename)
                if new_file_name != filename:
                    new_path = os.path.join(dir_name, new_file_name)
                    _do_rename(candidate, new_path)


if __name__ == "__main__":
    filespecs = _parse_command_line(sys.argv)
    for path in filespecs:
        _fix(path)

