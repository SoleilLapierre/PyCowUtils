"""Test path helper functions."""

import os

from ..path_utils import split_path, path_relative_to_script


def convert_slashes(path: str) -> str:
    """Canonicalize slashes for the OS."""
    return path.replace("/", os.path.sep)


def test_path_split_basic_relative() -> None:
    """Test splitting a relative path."""
    dirs, fname = split_path(convert_slashes("foo/bar/baz"))
    assert fname == "baz"
    assert dirs is not None
    assert len(dirs) == 2
    assert dirs[0] == "foo"
    assert dirs[1] == "bar"


def test_path_split_relative_no_filename() -> None:
    """Test splitting a relative path with only directory names."""
    dirs, fname = split_path(convert_slashes("foo/bar/baz/"))
    assert fname is None
    assert dirs is not None
    assert len(dirs) == 3
    assert dirs[0] == "foo"
    assert dirs[1] == "bar"
    assert dirs[2] == "baz"


def test_path_split_basic_absolute() -> None:
    """Test splitting an absolute path."""
    dirs, fname = split_path(convert_slashes("/foo/bar/baz"))
    assert fname == "baz"
    assert dirs is not None
    assert len(dirs) == 3
    assert dirs[0] == os.path.sep
    assert dirs[1] == "foo"
    assert dirs[2] == "bar"


def test_path_split_absolute_no_filename() -> None:
    """Test splitting an absolute path with no file name."""
    dirs, fname = split_path(convert_slashes("/foo/bar/baz/"))
    assert fname is None
    assert dirs is not None
    assert len(dirs) == 4
    assert dirs[0] == os.path.sep
    assert dirs[1] == "foo"
    assert dirs[2] == "bar"
    assert dirs[3] == "baz"


def test_path_split_relative_with_drive() -> None:
    """Test splitting a relative path with a drive letter."""
    dirs, fname = split_path(convert_slashes("c:foo/bar/baz"))
    assert fname == "baz"
    assert dirs is not None
    assert len(dirs) == 3
    assert dirs[0] == "c:"
    assert dirs[1] == "foo"
    assert dirs[2] == "bar"


def test_path_split_relative_no_filename_with_drive() -> None:
    """Test splitting a relative path with a drive letter and no file name."""
    dirs, fname = split_path(convert_slashes("c:foo/bar/baz/"))
    assert fname is None
    assert dirs is not None
    assert len(dirs) == 4
    assert dirs[0] == "c:"
    assert dirs[1] == "foo"
    assert dirs[2] == "bar"
    assert dirs[3] == "baz"


def test_path_split_basic_absolute_with_drive() -> None:
    """Test splitting an absolute path with a drive letter."""
    dirs, fname = split_path(convert_slashes("c:/foo/bar/baz"))
    assert fname == "baz"
    assert dirs is not None
    assert len(dirs) == 4
    assert dirs[0] == "c:"
    assert dirs[1] == os.path.sep
    assert dirs[2] == "foo"
    assert dirs[3] == "bar"


def test_path_split_absolute_no_filename_with_drive() -> None:
    """Test splitting an absolute path with a drive letter and no file name."""
    dirs, fname = split_path(convert_slashes("c:/foo/bar/baz/"))
    assert fname is None
    assert dirs is not None
    assert len(dirs) == 5
    assert dirs[0] == "c:"
    assert dirs[1] == os.path.sep
    assert dirs[2] == "foo"
    assert dirs[3] == "bar"
    assert dirs[4] == "baz"


def test_path_relative_to_script() -> None:
    """Test finding a path relative to the calling script."""
    path = path_relative_to_script(__file__, "__init__.py")
    assert "test_path_helper.py" not in path
    assert "__init__.py" in path
    assert os.path.isfile(path)
